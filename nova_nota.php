<!DOCTYPE html>
<html>
<head>
    <title>Nova Nota</title>
    <link rel="stylesheet" type="text/css" href="estilos/estilo_inputs.css">
</head>
<body>

    <!-- PROCESS -->
    <?php require_once("process.php")?>

    <form method="POST" action="process.php">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <label>Título:</label>
        <input type="text" name="titulo" value="<?php echo $titulo; ?>" required maxlength="500" placeholder=" Introduza o título da sua nota">
        <label>Nota:</label>
        <textarea name="info" id="nota" placeholder=" Introduza a sua nota"><?php echo $info; ?></textarea>

        <?php 
            if ($update == true):
        ?>

        <input type="submit" name="actualizar" value="Actualizar">

        <?php 
            else:
        ?>

        <input type="submit" name="gravar" value="Criar Nota">

        <?php 
            endif;
        ?> 
         
        <!-- <input type="submit" name="gravar" value="Criar Nota"> -->
    </form>
</body>
</html>

<?php
    $host = 'localhost';
    $utilizador = 'root';
    $senha = '';
    $bd = 'notes';

    $mysqli = mysqli_connect($host,$utilizador,$senha,$bd) or die(mysql_error($mysqli));
    // $user = 8;
    

    
   

    

    session_start();
    $resultado = $mysqli->query("SELECT * FROM note ORDER BY id DESC") or die($mysqli->error);

    // $user = $_SESSION['id'];
    $ut = $_SESSION['utilizador'];
?>

<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DaNote</title>
        <link rel="stylesheet" href="estilos/estilo.css">
    </head>
    <body>

        <header>
            <div class="titulo">
                <h4>Bloco de Notas Digital</p>    
            </div>
            

            <div class="informacao">
                <?php
                    if (isset($_SESSION['utilizador'])):
                ?>
                    <p id="user"><?php echo 'Utilizador: '.$ut?></p>
                <?php
                    endif;
                ?>

                <a href="logout.php">Encerrar Sessão</a>
            </div>
           

        </header>

         <!-- MENSAGEM: CRIADA COM SUCESSO -->

            <?php 
                if(isset($_SESSION['criar'])):
            ?>
                <p id="s_criar">Nota Criada Com Sucesso</p>

            <?php
                endif;
            ?>

            <!-- MENSAGEM EXCLUÍDA COM SUCESSO -->

            <?php
                if(isset($_SESSION['excluir'])):
            ?>
                <p id="s_excluir">Nota Excluída Com Sucesso</p>
            <?php
                endif;
            ?>
            
            <!-- MENSAGEM ACTUALIZADA COM SUCESSO -->
            <?php
                if(isset($_SESSION['actualizar'])):
            ?>
                <p id="s_actualizar">Nota Actualizada Com Sucesso</p>
            <?php
                endif;
            ?> 



  

        <?php while ($row = $resultado->fetch_array()):?>
            <tr>
                <div class="container">

                    <p id="titulo"><?php echo $row['titulo'];?></p>

                    <?php
                        if (strlen($row['info']) < 85):
                    ?>

                        <p class="info"> <?php echo $row['info']?></p>

                    <?php else:
                    ?>
                        <p class="info"><?php echo substr($row['info'], 0, 85).'...';?></p> 
                    <?php
                        endif;
                    ?>
                    
               

                <div class="accao">

                    <a id="visualizar" href="visualizar.php?view=<?php echo $row['id']; ?>">Visualizar</a>

                    <a id="editar" href="nova_nota.php?edit=<?php echo $row['id']; ?>">Editar</a>
                
                    <a id="eliminar" href="process.php?delete=<?php echo $row['id']; ?>">Eliminar</a>

                </div>
                    

            </div>
            <?php endwhile;?>



        <!-- PROCESS -->
        <?php require_once("process.php")?>
        
        <form action="process.php" method="post">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <input type="submit" id="nova_nota" name="criar_nota" value="Nova Nota">
        </form>
    </body>
</html>

